### Important
---
- This mod requires [AceCoreLib](https://gitlab.com/accensi/hd-addons/acecorelib).

### Usage
---
- Use Item: Drop roomba to collect brass or materials for the [Universal Reloader](https://gitlab.com/accensi/hd-addons/universal-reloader).
- Sprint + Use Item: Same as above but yeet it.
- To pick it up, double-tap Use on it.

### Notes
---
- Roombas can only be found in backpacks. They are also sold by the [Merchant](https://gitlab.com/accensi/hd-addons/merchant) if you have that loaded.
- The loadout code for the roomba is `rmb`.